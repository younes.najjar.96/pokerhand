# PokerHands
A poker hands evaluator, that tells you which hand is winning.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.6.

## Demo

You can run the [Demo here](https://poker.younesnajjar.me)

## Development server

Run `npm install` to install the missing dependencies.
Run `npm start` and you will have a running server on `http://localhost:4200/`.

## Running unit tests

Before running the unit testing, run a local build using the command line `ng build`.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

All the logic test cases you used are the file logic.specs.ts in the tests directory.
