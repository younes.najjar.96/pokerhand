import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComparingRoutingModule } from './comparing-routing.module';
import { ComparingComponent } from './comparing.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [ComparingComponent],
  imports: [
    CommonModule,
    ComparingRoutingModule,
    SharedModule
  ]
})
export class ComparingModule { }
