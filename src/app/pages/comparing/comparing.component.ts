import {Component, OnInit} from '@angular/core';
import {ScoringService} from '../../shared/services/scoring.service';
import {SelectService} from '../../shared/services/select.service';

@Component({
  selector: 'app-comparing',
  templateUrl: './comparing.component.html',
  styleUrls: ['./comparing.component.scss']
})
export class ComparingComponent implements OnInit {

  constructor(private selectService: SelectService) {
  }

  ngOnInit(): void {
  }

  reset(): void {
    this.selectService.reset();
  }
}
