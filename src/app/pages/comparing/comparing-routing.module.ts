import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ComparingComponent} from './comparing.component';

const routes: Routes = [{
  path: '',
  component: ComparingComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComparingRoutingModule { }
