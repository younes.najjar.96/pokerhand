import {TestBed} from '@angular/core/testing';
import {Card} from '../shared/models/card.model';
import {stringsToCards} from '../shared/utils/cards.utils';
import {calculate, score} from '../shared/utils/hands.functions';
import {BestHand} from '../shared/models/best-hand.model';

////////////////////////////////// Cases provided by Coding dojo //////////////////////////////////

describe('Cases provided by Coding dojo', () => {

  it('"H2 D3 S5 C9 DK" vs "C2 H3 S4 C8 HA" -> should be "C2 H3 S4 C8 HA" wins with high card', () => {
    expect(getWinnerText('H2 D3 S5 C9 DK', 'C2 H3 S4 C8 HA')).toBe('C2 H3 S4 C8 HA wins with high card');
  });
  it('"H2 S4 C4 D2 H4" vs "S2 S8 SA SQ S3" -> should be "H2 S4 C4 D2 H4" with full house', () => {
    expect(getWinnerText('H2 S4 C4 D2 H4', 'S2 S8 SA SQ S3')).toBe('H2 S4 C4 D2 H4 wins with full house');
  });
  it('"H2 D3 S5 C9 DK" vs "C2 H3 S4 C8 HK" -> should be "H2 D3 S5 C9 DK" with high card', () => {
    expect(getWinnerText('H2 D3 S5 C9 DK', 'C2 H3 S4 C8 HK')).toBe('H2 D3 S5 C9 DK wins with high card');
  });
  it('"H2 D3 S5 C9 DK" vs "D2 H3 C5 S9 HK" -> should be Tie', () => {
    expect(getWinnerText('H2 D3 S5 C9 DK', 'D2 H3 C5 S9 HK')).toBe('Tie');
  });
});


////////////////////////////////// Different Levels Cases //////////////////////////////////


describe('Different Levels Cases', () => {
  /******************  Royal flush vs Four of kind  *************************/
  it('"HA HQ HJ HT HK" vs "H2 D2 H3 S2 C2" -> Should be HA HQ HJ HT HK with royal flush (Royal flush vs Four of kind)', () => {
    expect(getWinnerText('HA HQ HJ HT HK', 'H2 D2 H3 S2 C2')).toBe('HA HQ HJ HT HK wins with royal flush');
  });

  /******************  Royal flush vs straight flush  *************************/
  it('"HA HQ HJ HT HK" vs "D9 DQ DJ DT DK" -> Should be HA HQ HJ HT HK with royal flush (Royal flush vs straight flush)', () => {
    expect(getWinnerText('HA HQ HJ HT HK', 'D9 DQ DJ DT DK')).toBe('HA HQ HJ HT HK wins with royal flush');
  });

  /******************  straight flush vs Four of kind  *************************/
  it('"H9 HQ HJ HT HK" vs "H2 D2 H3 S2 C2" -> Should be H9 HQ HJ HT HK with straight flush (straight flush vs Four of kind)', () => {
    expect(getWinnerText('H9 HQ HJ HT HK', 'H2 D2 H3 S2 C2')).toBe('H9 HQ HJ HT HK wins with straight flush');
  });
  /******************  Full house vs Four of kind  *************************/
  it('"D9 H9 C9 D4 C4" vs "H2 D2 H3 S2 C2" -> Should be H2 D2 H3 S2 C2 wins with four of a kind (Full house vs Four of kind)', () => {
    expect(getWinnerText('D9 H9 C9 D4 C4', 'H2 D2 H3 S2 C2')).toBe('H2 D2 H3 S2 C2 wins with four of a kind');
  });
  /******************  Full house vs flush  *************************/
  it('"D9 H9 C9 D4 C4" vs "H5 H6 HA H8 HT" -> Should be D9 H9 C9 D4 C4 wins with full house (Full house vs flush)', () => {
    expect(getWinnerText('D9 H9 C9 D4 C4', 'H5 H6 HA H8 HT')).toBe('D9 H9 C9 D4 C4 wins with full house');
  });
  /******************  straight vs flush  *************************/
  it('"D3 H4 C5 D6 C7" vs "H5 H6 HA H8 HT" -> Should be H5 H6 HA H8 HT wins with flush (straight vs flush)', () => {
    expect(getWinnerText('D3 H4 C5 D6 C7', 'H5 H6 HA H8 HT')).toBe('H5 H6 HA H8 HT wins with flush');
  });

  /******************  straight vs three of a kind  *************************/
  it('"D3 H4 C5 D6 C7" vs "H5 D5 S5 H8 HT" -> Should be D3 H4 C5 D6 C7 wins with straight (straight vs three of a kind)', () => {
    expect(getWinnerText('D3 H4 C5 D6 C7', 'H5 D5 S5 H8 HT')).toBe('D3 H4 C5 D6 C7 wins with straight');
  });
  /******************  two pair vs three of a kind  *************************/
  it('"D3 H3 CK D6 HK" vs "H5 D5 S5 H8 HT" -> Should be H5 D5 S5 H8 HT wins with three of a kind (two pair vs three of a kind)', () => {
    expect(getWinnerText('D3 H3 CK D6 HK', 'H5 D5 S5 H8 HT')).toBe('H5 D5 S5 H8 HT wins with three of a kind');
  });
  /******************  two pair vs one pair  *************************/
  it('"D3 H3 CK D6 HK" vs "D4 H4 CK D6 HA" -> Should be D3 H4 C5 D6 C7 wins with three of a kind (two pair vs one pair)', () => {
    expect(getWinnerText('D3 H3 CK D6 HK', 'D4 H4 CK D6 HA')).toBe('D3 H3 CK D6 HK wins with two pair');
  });

  /******************  High card vs one pair  *************************/
  it('"D3 H4 CK D6 HA" vs "D4 H4 CK D6 H7" -> Should be D4 H4 CK D6 H6 wins with three of a kind (high card vs one pair)', () => {
    expect(getWinnerText('D3 H4 CK D6 HA', 'D4 H4 CK D6 H7')).toBe('D4 H4 CK D6 H7 wins with one pair');
  });
});


//////////////////////////////////  //////////////////////////////////

describe('Same Level cases', () => {
  /******************  straight flush vs bigger straight flush *************************/
  it('"H8 H9 HT HJ HQ" vs "H9 HQ HJ HT HK" -> Should be H9 HQ HJ HT HK with straight flush (straight flush vs bigger straight flush)', () => {
    expect(getWinnerText('H8 H9 HT HJ HQ', 'D9 DQ DJ DT DK')).toBe('D9 DQ DJ DT DK wins with straight flush');
  });
  /******************  four of kind vs bigger four of kind *************************/
  it('"H4 D4 S4 C4 HQ" vs "H7 D7 S7 C7 H3" -> Should be H7 D7 S7 C7 H3 with straight flush (Four of kind vs bigger four of kind)', () => {
    expect(getWinnerText('H4 D4 S4 C4 HQ', 'H7 D7 S7 C7 H3')).toBe('H7 D7 S7 C7 H3 wins with four of a kind');
  });
  /******************  three of kind vs bigger three of kind *************************/
  it('"H4 D4 S4 C3 HQ" vs "H7 D7 S7 C5 H3" -> Should be H7 D7 S7 C5 H3 with straight flush (Three of kind vs bigger three of kind)', () => {
    expect(getWinnerText('H4 D4 S4 C3 HQ', 'H7 D7 S7 C5 H3')).toBe('H7 D7 S7 C5 H3 wins with three of a kind');
  });
  /******************  straight vs bigger Straight *************************/
  it('"H4 D5 S6 C7 H8" vs "H7 D8 S9 CT HJ" -> Should be H7 D8 S9 CT HJ with straight flush (Straight vs bigger Straight)', () => {
    expect(getWinnerText('H4 D5 S6 C7 H8', 'H7 D8 S9 CT HJ')).toBe('H7 D8 S9 CT HJ wins with straight');
  });

  /******************  high card vs bigger high card *************************/
  it('"H4 D5 S8 C3 HQ" vs "H7 D6 S9 C5 HA" -> Should be H7 D6 S9 C5 HA with straight flush (High card vs bigger high card)', () => {
    expect(getWinnerText('H4 D5 S8 C3 HQ', 'H7 D6 S9 C5 HA')).toBe('H7 D6 S9 C5 HA wins with high card');
  });
});

function oneStringToCards(cardsString: string): Card[] {
  return stringsToCards(cardsString.split(' '));
}

function getBestHands(...handsCards: Card[][]): BestHand[] {
  const hands = handsCards.map(handCards => calculate(handCards));
  let bestHands: BestHand[] = [];
  for (const hand of hands) {
    if (bestHands.length === 0 || hand.value > bestHands[0].value) {
      bestHands = [hand];
    } else if (hand.value === bestHands[0].value) {
      bestHands.push(hand);
    }
  }
  return bestHands;
}

function getWinner(...decks: string[]): any {
  const handsCards = decks.map(deck => oneStringToCards(deck));
  return getBestHands(...handsCards);
}

function getWinnerText(...decks: string[]): string {
  const winningHands = getWinner(...decks);
  if (winningHands.length === 1) {
    const bestHand = winningHands[0];
    const bestHandFirstCard = bestHand.cards[0].suit + bestHand.cards[0].rank;
    const winnerDeck = decks.find((deck: string) => deck.includes(bestHandFirstCard));
    return winnerDeck + ' wins with ' + bestHand.name;
  } else if (winningHands.length > 1) {
    return 'Tie';
  } else {
    return 'none';
  }
}
