import {Component, OnInit} from '@angular/core';
import {cardsObject} from '../../consts/cards.const';
import {SelectService} from '../../services/select.service';
import {SelectedCard} from '../../models/selection.model';
import {Player} from '../../models/player.model';

@Component({
  selector: 'app-cards-list',
  templateUrl: './cards-list.component.html',
  styleUrls: ['./cards-list.component.scss']
})
export class CardsListComponent implements OnInit {

  public cards: any = cardsObject;
  public Object = Object;
  private usedCards: string[] = [];

  constructor(private selectService: SelectService) {
    selectService.usedCards.subscribe(usedCards => {
      this.usedCards = usedCards;
    });
  }

  ngOnInit(): void {
  }

  setCard(card: any): void {
    if (!this.isUsed(card)) {
      this.selectService.addToUsed(card);
      this.selectService.setCurrentCard(card);
    }
  }

  isUsed(card: any): boolean {
    return this.usedCards.indexOf(card) >= 0;
  }
}
