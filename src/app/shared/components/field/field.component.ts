import {Component, OnDestroy, OnInit} from '@angular/core';
import {HEIGHT, WIDTH} from '../../consts/dimensions.consts';
import {getPlayerPosition} from '../../utils/dimentions.functions';
import {SelectService} from '../../services/select.service';
import {Player} from '../../models/player.model';
import {ScoringService} from '../../services/scoring.service';
import {superCopy} from '../../utils/general.functions';
import {BestHand} from '../../models/best-hand.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit, OnDestroy {

  public width = WIDTH;
  public height = HEIGHT;

  public players: Player[] = [];
  public scores: BestHand[] = [];
  public playersPositions: any[] = [];

  private playersSubscription: Subscription;
  private scoresSubscription: Subscription;

  constructor(private selectService: SelectService) {
    this.playersSubscription = selectService.players.subscribe((players => {
      this.players = superCopy(players);
    }));
    this.scoresSubscription = selectService.scores.subscribe((scores => {
      this.scores = superCopy(scores);
    }));
  }

  ngOnInit(): void {
    this.setPlayersPositions();
  }

  setPlayersPositions(): void {
    for (const player of this.players) {
      this.playersPositions.push(getPlayerPosition(player.number as number));
    }
  }

  ngOnDestroy(): void {
    if (this.playersSubscription) {
      this.playersSubscription.unsubscribe();
    }
    if (this.scoresSubscription) {
      this.scoresSubscription.unsubscribe();
    }
  }

}
