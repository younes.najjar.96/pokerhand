import {Component, OnDestroy, OnInit} from '@angular/core';
import {SelectService} from '../../services/select.service';
import {SelectedCard} from '../../models/selection.model';
import {ScoringService} from '../../services/scoring.service';
import {superCopy} from '../../utils/general.functions';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-flop',
  templateUrl: './flop.component.html',
  styleUrls: ['./flop.component.scss']
})
export class FlopComponent implements OnInit, OnDestroy {

  public flop: string[] | null[] | any = [];

  private selectedCard: SelectedCard | any;
  private selectedCardSubscription: Subscription;

  constructor(private selectService: SelectService) {
    this.selectService.flop.subscribe(flop => {
      this.flop = superCopy(flop);
    });
    selectService.selectedCard.subscribe(selectedCard => {
      this.selectedCard = selectedCard;
    });
  }

  ngOnInit(): void {
  }

  selectCard(handCardIndex: number): void {
    this.selectService.setSelectedCard('F', handCardIndex);
  }

  isSelectedCard(index: number): boolean {
    return this.selectedCard.container === 'F' && index === this.selectedCard.index;
  }

  ngOnDestroy(): void {
    if (this.selectedCardSubscription) {
      this.selectedCardSubscription.unsubscribe();
    }
  }

}
