import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {SelectService} from '../../services/select.service';
import {Player} from '../../models/player.model';
import {SelectedCard} from '../../models/selection.model';
import {ScoringService} from '../../services/scoring.service';
import {BestHand} from '../../models/best-hand.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, OnDestroy {
  @Input() player: Player | any;
  @Input() score: BestHand | any;

  public selectedCard: SelectedCard | any;
  private selectedCardSubscription: Subscription;


  constructor(private selectService: SelectService, public scoringService: ScoringService) {
    selectService.selectedCard.subscribe(selectedCard => {
      this.selectedCard = selectedCard;
    });
  }

  ngOnInit(): void {
  }

  enablePlayer(): void {
    this.selectService.changePlayerStatus(this.player.number as number);
  }

  selectCard(handCardIndex: number): void {
    this.selectService.setSelectedCard('P', handCardIndex, this.player.number);
  }

  isSelectedCard(index: number): boolean {
    return this.player.number === this.selectedCard.playerNumber && index === this.selectedCard.index;
  }

  get isWinner(): boolean {
    return this.scoringService.winners.length === 1 && this.scoringService.winners.includes(this.player.number as number);
  }
  get isDraw(): boolean {
    return this.scoringService.winners.length > 1 && this.scoringService.winners.includes(this.player.number as number);
  }

  ngOnDestroy(): void {
    if (this.selectedCardSubscription) {
      this.selectedCardSubscription.unsubscribe();
    }
  }
}
