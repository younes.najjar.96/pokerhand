import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {getCardUrl} from '../../utils/cards.utils';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnChanges {
  @Input() card: string | undefined;
  @Input() isSelected = false;
  @Input() isUsed = false;
  @Input() mode: 'small' | 'big' = 'big';
  public cardUrl = '';

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.setCardUrl();
  }

  setCardUrl(): void {
    this.cardUrl = getCardUrl(this.card as string);
  }

}
