import {Injectable} from '@angular/core';
import {SelectService} from './select.service';
import {Player} from '../models/player.model';
import {score} from '../utils/hands.functions';
import {stringsToCards} from '../utils/cards.utils';
import {superCopy} from '../utils/general.functions';

@Injectable({
  providedIn: 'root'
})
export class ScoringService {

  public players: Player[] = [];
  public flop: string[] | null[] = [];

  public winners: number[] = [];

  constructor(private selectService: SelectService) {
    selectService.players.subscribe(players => {
        this.players = superCopy(players);
        this.checkReadiness();
    });
    selectService.flop.subscribe(flop => {
        this.flop = superCopy(flop);
        this.checkReadiness();
    });
  }

  startScoring(): void {
    let maxScore = -1;
    let winners: number[] = [];
    const playersCopy = superCopy(this.players);
    const scores = [];
    for (const player of playersCopy) {
      if (player.isEnabled && player.number) {
        player.bestHand = score(stringsToCards(this.flop as string[]), stringsToCards(player.cards as string[]));
        scores.push(player.bestHand);
        if (player.bestHand.value > maxScore) {
          maxScore = player.bestHand.value;
          winners = [player.number];
        } else if (player.bestHand.value === maxScore) {
          winners.push(player.number);
        }
      } else {
        scores.push(null);
      }
    }
    this.winners = winners;
    this.selectService.updateScores(scores);
  }

  haveSameCards(player1: Player | any, player2: Player | any): boolean {
    for (let i = 0; i < (player1.cards as string[]).length; i++) {
      if (player1.cards[i] !== player2.cards[i]) {
        return false;
      }
    }
    return true;
  }

  checkReadiness(): void {
    const isEveryDataSet = !this.players
        .filter(player => player.isEnabled).map(player => (player.cards as any).includes(null)).includes(true)
      && !(this.flop as any).includes(null);
    if (isEveryDataSet) {
      this.startScoring();
    }
  }
}
