import {Injectable} from '@angular/core';
import {Player} from '../models/player.model';
import {BehaviorSubject} from 'rxjs';
import {getInitialPlayers} from '../utils/players.functions';
import {SelectedCard} from '../models/selection.model';
import {superCopy} from '../utils/general.functions';
import {BestHand} from '../models/best-hand.model';

@Injectable({
  providedIn: 'root'
})
export class SelectService {

  private initSelect: SelectedCard = {container: 'P', index: 0, playerNumber: 1};
  private initFlop = [null, null, null, null, null];
  public players: BehaviorSubject<Player[]> = new BehaviorSubject<Player[]>(getInitialPlayers());
  public flop: BehaviorSubject<string[] | null[]> = new BehaviorSubject<string[] | null[]>(this.initFlop);
  public selectedCard: BehaviorSubject<SelectedCard> = new BehaviorSubject<SelectedCard>(superCopy(this.initSelect));
  public usedCards: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  public scores: BehaviorSubject<BestHand[]> = new BehaviorSubject<BestHand[]>([]);


  constructor() {
  }

  changePlayerStatus(playerNumber: number): void {
    const playerIndex = this.players.value.findIndex(player => player.number === playerNumber);
    this.players.value[playerIndex].isEnabled = !this.players.value[playerIndex].isEnabled;
    this.players.next(this.players.value);
  }

  setSelectedCard(container: 'P' | 'F', index: number, playerNumber?: number): void {
    this.selectedCard.next({container, index, playerNumber});
  }

  setCurrentCard(card: string): void {
    const selectedCard = this.selectedCard.value;
    const playerCopy: Player[] = superCopy(this.players.value);
    if (selectedCard.container === 'P') {
      const index = playerCopy.findIndex(player => player.number === selectedCard.playerNumber);
      (playerCopy[index].cards as string[])[selectedCard.index as number] = card;
      this.players.next(playerCopy);
    } else if (selectedCard.container === 'F') {
      const flopCopy = superCopy(this.flop.value);
      flopCopy[selectedCard.index as number] = card;
      this.flop.next(flopCopy);
    }
    this.moveToNext();
  }

  addToUsed(newCard: string): void {
    const usedCards = this.usedCards.value;
    const previousCard = this.getPreviousCard();
    if (previousCard) {
      usedCards.splice(usedCards.indexOf(previousCard), 1);
    }
    usedCards.push(newCard);
  }

  getPreviousCard(): string {
    const selectedCard = this.selectedCard.value;
    let previousCard;
    if (selectedCard.container === 'P') {
      const index = this.players.value.findIndex(player => player.number === selectedCard.playerNumber);
      previousCard = (this.players.value[index].cards as any[])[selectedCard.index as number];
    } else {
      previousCard = this.flop.value[selectedCard.index as number];
    }
    return previousCard;
  }

  private moveToNext(): void {
    const selectedCard = this.selectedCard.value;
    if (selectedCard.container === 'P') {
      const nextAvailableCard = this.getNextAvailableCard();
      if (nextAvailableCard) {
        this.selectedCard.next(nextAvailableCard);
      } else {
        this.selectedCard.next({container: 'F', index: 0});
      }
    } else {
      if (selectedCard.index as number <= 3) {
        (selectedCard.index as number)++;
        this.selectedCard.next(selectedCard);
      } else {
        this.selectedCard.next(superCopy(this.initSelect));
      }
    }
  }

  getNextAvailableCard(): SelectedCard | null {
    if (this.selectedCard.value.index === 0) {
      this.selectedCard.value.index++;
      return this.selectedCard.value;
    } else {
      const currentPlayerIndex = this.players.value.findIndex(player => player.number === this.selectedCard.value.playerNumber);
      const nextPlayer = this.players.value.find((player, index) => index > currentPlayerIndex && player.isEnabled);
      if (nextPlayer) {
        return {container: 'P', index: 0, playerNumber: nextPlayer.number};
      }
    }
    return null;
  }

  updateScores(scores: BestHand[]): void {
    this.scores.next(scores);
  }

  reset(): void {
    this.players.next(getInitialPlayers());
    this.flop.next(this.initFlop);
    this.selectedCard.next(superCopy(this.initSelect));
    this.usedCards.next([]);
    this.scores.next([]);
  }
}
