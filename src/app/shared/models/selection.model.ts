export interface SelectedCard {
  container?: 'P' | 'F'; // player or flop
  playerNumber?: number;
  index?: number; // Index in the container
}
export function isPlayer(type: string): boolean {
  return type === 'P';
}
export function isContainer(type: string): boolean {
  return type === 'P';
}
