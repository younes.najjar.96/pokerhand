import {Card} from './card.model';

export interface BestHand {
  cards: Card[];
  name: string;
  value: number;
}
