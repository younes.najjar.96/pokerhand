import {BestHand} from './best-hand.model';

export interface Player {
  number?: number;
  cards?: string[] | null[];
  isEnabled?: boolean;
  bestHand?: BestHand;
}
