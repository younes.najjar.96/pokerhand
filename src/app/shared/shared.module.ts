import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldComponent } from './components/field/field.component';
import { PlayerComponent } from './components/player/player.component';
import { CardComponent } from './components/card/card.component';
import { FlopComponent } from './components/flop/flop.component';
import {SelectService} from './services/select.service';
import { CardsListComponent } from './components/cards-list/cards-list.component';
import {ScoringService} from './services/scoring.service';



@NgModule({
  declarations: [FieldComponent, PlayerComponent, CardComponent, FlopComponent, CardsListComponent],
  imports: [
    CommonModule
  ],
  exports: [FieldComponent, PlayerComponent, CardComponent, CardsListComponent],
  providers: [SelectService, ScoringService]
})
export class SharedModule { }
