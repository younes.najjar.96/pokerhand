export function superCopy(item: any): any {
  return JSON.parse(JSON.stringify(item));
}
