import {BestHand} from '../models/best-hand.model';
import {Card} from '../models/card.model';
import {ranks} from '../consts/cards.const';


export function score(...allCards: Card[][]): BestHand {
  // return the best poker hand from a set or sets of cards
  const cards = sanitise(allCards);
  // start empty
  let best = finalHand(cards);

  // find best hand
  for (const combination of combinationsList(cards, 5)) {
    // calculate value of 5 cards
    const result = calculate(combination);
    if (result.value > best.value) {
      best = result;
    }
  }
  // finish with best result
  return best;
}


function finalHand(cards: Card[], name?: string, value?: number): BestHand {
  return {
    cards,
    name: name || 'nothing',
    value: value || 0
  };
}

function sanitise(allCards: Card[][]): Card[] {
  // concatenate
  let cards: Card[] = [].concat.apply([], allCards as any);

  // valid rank and suit
  cards = cards.filter((card) => {
    return !!(ranks.indexOf(card.rank) > -1 && card.suit);
  });
  return cards;
}

function combinationsList(array: Card[], arraySize: number): Card[][] {
  const resultSet = [];
  let result;
  for (let x = 0; x < Math.pow(2, array.length); x++) {
    result = [];
    let i = array.length - 1;
    do {
      // tslint:disable-next-line:no-bitwise
      if ((x & (1 << i)) !== 0) {
        result.push(array[i]);
      }
    } while (i--);

    if (result.length === arraySize) {
      resultSet.push(result);
    }
  }
  return resultSet;
}

function rankRegroup(cards: Card[]): Card[][] {
  /**
   * @goal: regroup cards based on rank and sort based on group length, ex: ['2H','3H','4D','4H','8C'] => [['4D','4H'],['2H'],['3H'],['8C']]
   * @note: the cards are automatically sorted based on rank using this logic
   */
    // split cards by rank
  let result: Card[][] = [];

  for (const card of cards) {
    const r = ranks.indexOf(card.rank);
    result[r] = result[r] || [];
    result[r].push(card);
  }

  // condense
  result = result.filter((rank) => !!rank);
  // high to low
  result.reverse();

  // pairs and sets first
  result.sort((a, b) => {
    return a.length > b.length ? -1 : a.length < b.length ? 1 : 0;
  });

  return result;
}

function getIsFlush(cards: Card[]): boolean {
  // all suits match is flush
  const suit = cards[0].suit;

  for (const card of cards) {
    if (card.suit !== suit) {
      return false;
    }
  }

  return true;
}

function getIsStraight(ranked: Card[][]): boolean {
  // must have 5 different card ranks
  if (ranked.length !== 5) {
    return false;
  }
  // Checking if it's a wheel (a wheel is when you have a straight of A,2,3,4,5, the A is considered as a member of suit)
  if (ranked[0][0].rank === 'A' && ranked[1][0].rank === '5' && ranked[4][0].rank === '2') {
    // hand is 'Ace' '5' '4' '3' '2'
    ranked.push(ranked.shift() as Card[]);
    // hand is '5' '4' '3' '2' 'Ace'
    return true;
  }

  // Run of five in row is straight
  const firstRank = ranks.indexOf(ranked[0][0].rank);
  const lastRank = ranks.indexOf(ranked[4][0].rank);
  return (firstRank - lastRank) === 4;
}

function importanceValue(ranked: Card[][], primary: number): number {
  // The biggest importance value wins
  let str = '';

  for (const rank of ranked) {
    // create two digit value
    const r = ranks.indexOf(rank[0].rank);
    const v = (r < 10 ? '0' : '') + r;
    for (let i = 0; i < rank.length; i++) {
      // append value for each card
      str += v;
    }
  }
  // Highest primary wins automatically, for the ones with the same primary we compare for the highest cards
  return (primary * 10000000000) + parseInt(str, undefined);
}

export function calculate(cards: Card[]): BestHand {
  // determine value of hand

  const ranked: Card[][] = rankRegroup(cards);
  const isFlush = getIsFlush(cards);
  const isStraight = getIsStraight(ranked);

  if (isStraight && isFlush && ranked[0][0].rank === 'A' && ranked[1][0].rank === 'K') {
    return finalHand(cards, 'royal flush', importanceValue(ranked, 9));
  } else if (isStraight && isFlush) {
    return finalHand(cards, 'straight flush', importanceValue(ranked, 8));
  } else if (ranked[0].length === 4) {
    return finalHand(cards, 'four of a kind', importanceValue(ranked, 7));
  } else if (ranked[0].length === 3 && ranked[1].length === 2) {
    return finalHand(cards, 'full house', importanceValue(ranked, 6));
  } else if (isFlush) {
    return finalHand(cards, 'flush', importanceValue(ranked, 5));
  } else if (isStraight) {
    return finalHand(cards, 'straight', importanceValue(ranked, 4));
  } else if (ranked[0].length === 3) {
    return finalHand(cards, 'three of a kind', importanceValue(ranked, 3));
  } else if (ranked[0].length === 2 && ranked[1].length === 2) {
    return finalHand(cards, 'two pair', importanceValue(ranked, 2));
  } else if (ranked[0].length === 2) {
    return finalHand(cards, 'one pair', importanceValue(ranked, 1));
  } else {
    return finalHand(cards, 'high card', importanceValue(ranked, 0));
  }

}
