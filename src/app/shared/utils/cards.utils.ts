import {cardTypes} from '../consts/cards.const';
import {Card} from '../models/card.model';

export function getCardUrl(card: string): string {
  if (!card) {
    return 'assets/blue-back.png';
  }
  const cardType = cardTypes.find(type => card[0] === type[0]);
  return 'assets/' + (cardType as string).toLowerCase() + '/' + card.slice(1) + '.png';
}

export function stringsToCards(cardStrings: string[]): Card[] {
  return cardStrings.map(cardString => {
    return {suit: cardString.slice(0, 1), rank: cardString.slice(1)};
  });
}
export function cardsToStrings(cards: Card[]): string[] {
  return cards.map(card => {
    return card.suit + card.rank;
  });
}
