import {Player} from '../models/player.model';
import {superCopy} from './general.functions';

export function getInitialPlayers(): Player[] {
  const players: Player[] = [];
  for (let i = 1; i <= 8; i++) {
    players.push({
      number: i,
      cards: [null, null],
      isEnabled: false
    });
  }
  players[0].isEnabled = true;
  players[1].isEnabled = true;
  return superCopy(players);
}
