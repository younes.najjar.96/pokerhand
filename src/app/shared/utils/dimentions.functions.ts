import {PLAYERS_POSITION_OFFSET, WIDTH} from '../consts/dimensions.consts';

export function getPlayerPosition(playerNumber: number): number[] {
  const xAxesPositions = [-0.7, 0, 0.7, 1, 0.7, 0, -0.7, -1];
  const xAxesPosition = xAxesPositions[playerNumber - 1] * (WIDTH + PLAYERS_POSITION_OFFSET);
  const yAxesPosition = getYAxesPosition(playerNumber, xAxesPosition);
  return [xAxesPosition, yAxesPosition];
}

function getYAxesPosition(playerNumber: number, xAxesPosition: number): number {
  const yAxesPosition = Math.sqrt(Math.pow((WIDTH + PLAYERS_POSITION_OFFSET), 2) - Math.pow(xAxesPosition, 2)) / 2;
  return yAxesPosition * ((playerNumber <= 3) ? 1 : -1);
}
